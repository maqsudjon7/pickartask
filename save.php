<?php

$servername = "localhost";
$username = "u0595297_root";
$password = "V0k0N3c3";
$database = "u0595297_taskmanager";

$currency = [
    'EUR' => true,
    'USD' => true,
    'CHF' => true
];

$request = $_SERVER['REQUEST_METHOD'];

$link = mysqli_connect($servername, $username, $password, $database);



function get(): void 
{
    global $link;
    $query = "SELECT * from histories";
    $queryResult = mysqli_query($link, $query);
    if (!$queryResult) {
        die("Something went wrong! Please reload page and repeat!");
    }
    echo json_encode(mysqli_fetch_all($queryResult, MYSQLI_ASSOC));
}

function save($amount, $result, $fromCurrency, $toCurrency)
{
    global $link;
    $date = date('M d, YYYY');
    $query = "INSERT INTO histories VALUES (NULL, {$amount}, {$result}, '{$fromCurrency}', '{$toCurrency}', '{$date}')";
    $queryResult = mysqli_query($link, $query);

    if (!$queryResult) {
        mysqli_close($link);
        die("Something went wrong! Please reload page and repeat!");
    }

    echo "Your convertion was saved in database!";
}

function validate($amount, $result, $from, $to): bool
{
    global $currency;
    if (!is_numeric($amount) && !is_numeric($result)) {
        return false;
    } elseif (!isset($currency[$from]) && !isset($currency[$to])) {
        return false;
    }

    return true;
}


if ($request === "POST") {
    $amount = $_POST['amount'];
    $result = $_POST['result'];
    $fromCurrency = htmlspecialchars($_POST['from']) ;
    $toCurrency = htmlspecialchars($_POST['to']);

    if (!validate($amount, $result, $fromCurrency, $toCurrency)) {
        mysqli_close($link);
        die("Something went wrong! Please reload page and repeat1!");
    }

    save($amount, $result, $fromCurrency, $toCurrency);
} else {
    get();
}

mysqli_close($link);