$(document).ready(function () {
    let amount = $('#amount').val();

    let positions = {
        'EUR': 2,
        'USD': 1,
        'CHF': 3
    };

    $(`#to option:nth-child(2)`).attr('selected', true);

    $('#change-button').on('click', function () {
        let fromValue = $("#from").val();
        let toValue = $("#to").val();
        $("#from").val(toValue);
        $("#to").val(fromValue);
    });

    $('#amount').on('change', function () {
        let money = $(this).val();
        if (!isNumeric(money)) {
            $(this).val(1.0);
        } else if (length(money) >= 20) {
            alert('Amount should be less than 10000000000000000000'); 
            $(this).val(1.0);
        }
    });

    $('form').on('submit', function (e) {
        e.preventDefault();
        amount = $('#amount').val();
        let selectedValue = $("#from").val();
        let to = $("#to option:nth-child(2)").val();
        if (!(selectedValue in positions) && !(to in positions)) {
            alert('Wrong currency was selected')
            return false;
        }

        if (!isNumeric(amount)) {
            alert('Amount must be numeric!')
            return false;
        }

        let currency = `${selectedValue}_${to}`;
        $.get(`https://free.currconv.com/api/v7/convert?q=${currency}&compact=ultra&apiKey=058df4cffcfd6e7134e5`)
            .done(function (result) {
                result = amount * result[currency];
                $('#from-id').text(`${amount} ${selectedValue} =`);
                $('#to-id').text(`${result} ${to}`);
                $.post("/save.php", { amount, from: selectedValue, to, result: result.toFixed(5) }, function (result) {
                    alert(result);
                });
            });
    });

    $('#link').on('click', function () {
        $('#main-content').removeClass('d-flex');
        $('.logo').removeClass('flex-lg-grow-1');
        $('.content').removeClass('h-100');
        $('#main-content').hide();
        $('#main-title').hide();
        $('#history').show();
        $('.table-body').empty();
        $.get(`/save.php`)
            .done(function (result) {
                $.each(JSON.parse(result), function (key, data) {
                    let htmlElement = `
                    <div class="table-row">
                        <div class="table-body-cell">${data.date}</div>
                        <div class="table-body-cell">${data.amount} ${data.fromCurrency}</div>
                        <div class="table-body-cell">${data.result} ${data.toCurrency}</div>
                    </div>
                    `;
                    $('.table-body').append(htmlElement);
                })
            });
    });

    $('#link-back').on('click', function (e) {
        $('#main-content').addClass('d-flex');
        $('.content').addClass('h-100');
        $('.logo').addClass('flex-lg-grow-1');
        $('#main-content').show();
        $('#main-title').show();
        $('#history').hide();
    });

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function length(n) {
        return n.toString().length;
    }
});